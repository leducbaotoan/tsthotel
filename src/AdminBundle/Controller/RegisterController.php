<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use CoreBundle\Lib\RestApi\RestApi;
use Symfony\Component\HttpFoundation\Session\Session;

class RegisterController extends Controller
{
    public function indexAction(Request $request){
    	$session = $request->getSession();
    	$objLogin = null;
        $userId = $session->get('user');
    	if(!empty($userId)){
        	$result = RestApi::postData('account/userdetails', array($userId));
	   		return $this->render('admin/list_user.html.twig',array('userDetails' => $result));
    	}else{
    		$loginUrl = $this->generateUrl('login');
    		return $this->redirect($loginUrl);
    	}
    }
    public function registerAction(Request $request){
    	$session = $request->getSession();
    	$objLogin = null;
        $userId = $session->get('user');
    	if(!empty($userId)){
    		if($this->getRequest()->isMethod('POST')){
    			$data = $this->get('request')->request->all();
    			var_dump($data);exit;
    		}
        	$result = RestApi::postData('account/userdetails', array($userId));
	   		return $this->render('admin/register.html.twig',array('userDetails' => $result));
    	}else{
    		$loginUrl = $this->generateUrl('login');
    		return $this->redirect($loginUrl);
    	}
    }
}
