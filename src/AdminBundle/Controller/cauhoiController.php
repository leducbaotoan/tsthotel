<?php
/**
 * Created by PhpStorm.
 * User: AnTony
 * Date: 11/29/2016
 * Time: 8:44 PM
 */
use CoreBundle\Lib\Cache\RedisManager;
use CoreBundle\Lib\Security\AuthorizationProvider;
use CoreBundle\Lib\Utils\ServerResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\Tools\Pagination\Paginator;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Pagination\Pagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use CoreBundle\Lib\Log\Logger;

class cauhoiController extends Controller
{
    public function getcauhoiAction()
    {
        try{
            $limit = !empty($_GET['limit']) ? (int) $_GET['limit'] : 100;
            $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
            $data_search = array(
                'NumberPage' => 10,
                'NumberRow' => 0,
            );


            $cauhoi = RestApi::getData('/cauhoi/getlist', array(
                'NumberPage' => 10,
                'NumberRow' => 0
            ));
            $data		= isset($cauhoi['data'])?$cauhoi['data']:array();
            $request = $this->get('request');
            return $this->render('AdminBundle::.html.twig', array(
                    'list_data' => $data,
                    'data_search' => $data_search
                )
            );

        }catch (\Exception $ex)
        {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
    }
    public function getOnecauhoiAction($id)
    {
        try{
            $request = $this->get('request');
            $session = $request->getSession();
            if ($id != NULL || $id != 0) {

            }

            return $this->render('cauhoi/edit.html.twig', array(


            ));

        }catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }

    }
}