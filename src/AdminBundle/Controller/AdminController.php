<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use CoreBundle\Lib\RestApi\RestApi;

class AdminController extends Controller
{

    public function indexAction(Request $request) 
    {
    	$session = $request->getSession();
    	$objLogin = null;
        $userId = $session->get('user');
    	if(!empty($userId)){
        	$result = RestApi::postData('account/userdetails', array($userId));
	   		return $this->render('admin/admin.html.twig',array('userDetails' => $result));
    	}else{
    		$loginUrl = $this->generateUrl('login');
    		return $this->redirect($loginUrl);
    	}
    }
}
