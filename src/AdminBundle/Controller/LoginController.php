<?php
namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use CoreBundle\Lib\RestApi\RestApi;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginController extends Controller
{
    public function indexAction(Request $request){
        $searchParams   = array();
        $session = $request->getSession();
        $loginSession = $session->get('user');
            // var_dump($loginSession);exit;
        if( empty($loginSession) && $this->getRequest()->isMethod('POST') ){
            $searchParams['user_name']    = $request->request->get('username');
            $searchParams['password']     = $request->request->get('password');
            $objLogin = null;
            $objLogin = RestApi::postData('account/login', $searchParams);
            $result = $objLogin['data'];
            // var_dump($result);exit;
            if($result != false){
                $session->set('user', $result['UserID']);
                return $this->redirect($this->generateUrl('admin'));
            }
        }
       return $this->render('admin/login.html.twig');
    }

    /**
	 * @Route("/loginaction", name="loginaction")
	 */

    public function loginAction(Request $request){

    }

    public function logoutAction(Request $request){
        $session = $request->getSession();
        $session->remove('user');
        return $this->render('admin/login.html.twig');
    }

}