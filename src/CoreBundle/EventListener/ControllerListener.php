<?php

namespace CoreBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;

use CoreBundle\Lib\Config\ConfigManager;
use CoreBundle\Lib\Security\SecurityManager;
use CoreBundle\Lib\Security\AuthorizationProvider;
use CoreBundle\Lib\RestApi\RestApi;

/**
 * Business class for listening controller caller
 * @author KhoaNT
 */
class ControllerListener
{
	private $resolver;
	
	public function __construct($resolver)
    {
        $this->resolver = $resolver;
    }
	
    /*
     * This function use for listening an event of caller controller
     */
    public function onKernelController(FilterControllerEvent $event)
    {
    	$errMsg 		= "";
		$msgType		= 0; // 0: session timeout, 1: you dont have permission
		$username 		= '';
    	try {
			$request 		= $event->getRequest();
	       	$controller 	= $event->getController();
		   	$objController 	= $controller[0];
			$route  		= $request->attributes->get('_route');
			$session 		= $request->getSession();
			$arrRouting		= explode('_', $route);
			
			$loadMasterData = "";
			$getTypeData 	= $request->query->get('loadDataType');
			if(!empty($getTypeData))
				$loadMasterData = $getTypeData;

			// Init config
			ConfigManager::init();
			
			$pathController = $request->attributes->get("_controller");
			$startPos = strrpos($pathController , "\\");
			if ($startPos === false) {
				$paths = explode(":", $pathController);
	            $controllerName = $paths[1];
	        } else {
	            $controllerName = substr($pathController, $startPos + 1);
	            $controllerName = str_replace("Controller::".$controller[1], "", $controllerName);
			}
			
			$actionName = substr($controller[1], 0, strlen($controller[1])-6);
			$username = $session->get(AuthorizationProvider::CURRENTUSER);
			$token_login = $session->get(AuthorizationProvider::TOKEN_KEY);
			//echo $username.'==='.$token_login.'=='.$controllerName.'==='.$actionName;die;
			
			$isCheckLogin = false;			
			$isCheckLogin = SecurityManager::checkUserLogged($username, $token_login);
			
			$routing = 'home';
			if(AuthorizationProvider::authAction($actionName)) {
				$iAuthorization = 1;
				if($loadMasterData == "")
					$iAuthorization = SecurityManager::authorization($controllerName, $actionName, $username);
				if(!$isCheckLogin) {
					$msgType = -1;
					$errMsg = "Tài khoản của bạn đã quá thời gian sử dụng. Vui lòng đăng nhập lại";
				} else if($iAuthorization == -1) {
					$msgType = -2;
					
					if($arrRouting[1] == 'getone') {
						$msgType = -3;
						$routing = $arrRouting[0].'_index';
					} else if($arrRouting[1] == 'index'){
						$msgType = -5;
						$routing = 'home';
					}
					
					$errMsg = "Bạn không có quyền thực hiện chức năng này";
				}
			} else {
				if($isCheckLogin == false && $actionName !== 'login' && $actionName !== 'sigin') {
					$msgType = -4;
					$errMsg = "Tài khoản của bạn đã quá thời gian sử dụng. Vui lòng đăng nhập lại";
				}
			}
	
			if($errMsg != "") {
				$request->attributes->set('_controller', 'CoreBundle:Authentication:timeout');
				$request->attributes->set('errmsg', $errMsg);
				$request->attributes->set('msgType', $msgType);
				$request->attributes->set('route', $arrRouting[0]);
		        $authController = $this->resolver->getController($request);
		        $event->setController($authController);
			}
    	} catch(\Exception $ex) {
    		$msgType = -10;
    		$errMsg =  "System failure! ".$ex->getMessage();
			$request->attributes->set('_controller', 'CoreBundle:Authentication:timeout');
			$request->attributes->set('errmsg', $errMsg);
			$request->attributes->set('msgType', $msgType);
			$request->attributes->set('route', '');
	        $authController = $this->resolver->getController($request);
	        $event->setController($authController);
    	}
    }
} 