<?php
namespace CoreBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use CoreBundle\Lib\Log\Logger;
use CoreBundle\Lib\Config\ConfigManager;

class ExceptionListener
{
    protected $templating;
    protected $kernel;
    protected $logger;

    public function __construct(EngineInterface $templating, $kernel, $logger)
    {
        $this->templating = $templating;
        $this->kernel = $kernel;
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
    	ConfigManager::init();
        if ('prod' == $this->kernel->getEnvironment()) {
            // exception object
            static $handling;
            $exception = $event->getException();

            // new Response object
            if (true === $handling) {
		        return;
		    }
		    $handling = true;
	
			$message = $this->templating->render('exception/error500.html.twig', array());
			$response = new Response($message);
			$this->logger->errLog($exception);
            $event->setResponse($response);
			
			$handling = false;
        }
    }
}