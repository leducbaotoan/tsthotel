<?php
namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\Utils\PhpUtility;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Log\Logger;

/**
 * Ums_GroupController
 * @author KhoaNT
 */
class UMS_GroupController extends Controller
{
	private static function mappingFields() {
        return array(
        	'GroupID' => 'GroupID'
		);
    }
	
    /**
     * Get all list user
     */
    public function getGroupListAction()
    {
    	try {
    		$data = array();
	    	$request 					= $this->get('request');
			$session 					= $request->getSession();
			$searchParams				= array();
			$searchParams['limit']		= 100;
			$searchParams['q']	= 
				$request->request->get('q') != NULL ? $request->request->get('q') : NULL;
			
			$objMenuList = RestApi::getData('ums_group', $searchParams);
			$data['list_datas'] = $objMenuList['data']['datas'];
			
			$data['data_search'] = $searchParams;
			
			return $this->render('group/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }
	
	public function getOneGroupAction($id) {
		try {
            $data_info = array();
			$data_info['data'] = array();
			$data_info['title'] = 'Thêm mới';
			
			$data_company = RestApi::getData('ums_group', array('limit'=>100));
			//var_dump($data_company);die;
			$data_info['data_list'] = $data_company['data']['datas'];
			
            if ($id != NULL || $id != "") {
                $data = RestApi::getData('ums_group/get/' . $id);
				//var_dump($data);die;
				$data_info['data'] = $data['data'];
				$data_info['title'] = 'Cập nhật';
            } else {
            	$data_info['data'] = array('GroupID' => "", 'ParentGroupID' => "");
            }
            
            return $this->render('group/edit.html.twig', $data_info);
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
	
	public function saveGroupAction() {
		try {
            $resp 		= new ServerResponse();
            $request 	= $this->get('request');
			$session 	= $request->getSession();
			$groupid = $request->request->get('groupid');
			
            $reqdata = array(
				'ParentGroupID' => $request->request->get('parentgroupid'),
				'Description' => $request->request->get('groupname')
			);
			
			//var_dump($reqdata);die;
			
            $resultPost = array();

            if ($groupid != "") { // Update
                $resultPost = RestApi::postData('ums_group/update/' . trim($groupid), $reqdata);
                $resp->addActionType('update');
            } else { // Insert
            	$reqdata['GroupID'] = $request->request->get('groupcode');
                $resultPost = RestApi::postData('ums_group/create', $reqdata);
                $resp->addActionType('insert');
            }
			
            if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingFields());
                $resp->addErrorInfo('System', array('message' => $message));
            }

            $resp->addUrlPrevious($this->generateUrl('group_index'));
            return new Response(json_encode($resp));
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
}
