<?php
namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\Utils\PhpUtility;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Log\Logger;

/**
 * UMS_ActionModuleController
 * @author KhoaNT
 */
class UMS_ActionModuleController extends Controller
{
	private static function mappingActionFields() {
        return array(
        	'ActionID' => 'Action ID',
            'ActionName' => 'Action name'
		);
    }
	
	private static function mappingModuleFields() {
        return array(
        	'ModuleID' => 'Module'
		);
    }
	
    /**
     * Get all list user
     */
    public function getActionListAction()
    {
    	try {
    		$data = array();
	    	$request 					= $this->get('request');
			$session 					= $request->getSession();
			$searchParams				= array();
			$limit = !empty($_GET['limit']) ? (int)$_GET['limit'] : 20;
			$searchParams['limit']		= $limit;
			$current_page = !empty($_GET['page'])?(int)$_GET['page']:1;
			$searchParams['start']		= $limit*($current_page - 1);
	
			$searchParams['ModuleID']	= 
				$request->request->get('ModuleID') != NULL ? $request->request->get('ModuleID') : NULL;

			$data['data_search'] = $searchParams;
			$data['ModuleID'] = $searchParams['ModuleID'];

			$objActionList = RestApi::getData('ums_action/get-action', $searchParams);
			$data['datas'] = $objActionList['data'];
	
			// Get list role
			$data_module = RestApi::getData('ums_module', array('limit'=>150));
			$data['data_module'] = $data_module['data']['datas'];
			
			if($data != null) {
				$paging = new Pagination((int)count($objActionList['data']), $limit, $current_page);
				$page_div = $paging->render();
				$data['pagination'] = $page_div;
			}
			
			return $this->render('action/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }

/**
	 * Action that return save one instance of Cities
	 * 
	 * @return JSON message reponse from Server
	 * 
	 */
	public function saveModuleAction()
    {
    	try {
    		$resp = new ServerResponse();
			$request = $this -> get('request');
			$session = $request->getSession();
			$ModuleID = $request -> request -> get("ModuleID");

			if(!empty($ModuleID)) {
				$respData = array(
					'ModuleName' => $request -> request -> get("ModuleName")
				);
				$resultPost = RestApi::postData('ums_module/update/' . $ModuleID, $respData);
			} else { // Insert
            	$respData = array(
					'ModuleID' => $request -> request -> get("ModuleCode"),
					'ModuleName' => $request -> request -> get("ModuleName")
				);
                $resultPost = RestApi::postData('ums_module/create', $respData);
            }
			
            if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingModuleFields());
                $resp->addErrorInfo('System', array('message' => $message));
            }

            $resp->addUrlPrevious($this->generateUrl('functact_index'));
            return new Response(json_encode($resp));
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }

	/**
	 * Action that return save one instance of Cities
	 * 
	 * @return JSON message reponse from Server
	 * 
	 */
	public function saveActionAction()
    {
    	try {
    		$resp = new ServerResponse();
			$request = $this -> get('request');
			$session = $request->getSession();
			$ActionID = $request -> request -> get("actionid");

			$respData = array(
				'ModuleID' => $request -> request -> get("popmoduleid"),
				'ActionName' => $request -> request -> get("actionname"),
				'DisplayName' => $request -> request -> get("displayactionname"),
				'Routing' => $request -> request -> get("actionrouting"),
				'ActionType' => $request -> request -> get("actiontype"),
			);

			if (!empty($ActionID)) { // Update 
                $resultPost = RestApi::postData('ums_action/update/' . $ActionID, $respData);
            } else { // Insert
            	$respData['ActionID'] = $request -> request -> get("actionname");
                $resultPost = RestApi::postData('ums_action/create', $respData);
            }
			
            if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingActionFields());
                $resp->addErrorInfo('System', array('message' => $message));
            }

            $resp->addUrlPrevious($this->generateUrl('functact_index'));
            return new Response(json_encode($resp));
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
	}
	
}
