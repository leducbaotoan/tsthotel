<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use CoreBundle\Biz\PermisionBiz;
use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\EntityModule\EntityManager;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\Log\Logger;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Security\AuthorizationProvider;
use CoreBundle\Lib\Cache\RedisManager;

/**
 * Permission controller
 * @author KhoaNT
 */
class PermisionController extends Controller
{
    /**
     * Get all list user
     */
    public function getPermisionListAction()
    {
    	try {
    		$data = array();
	    	$request 			= $this->get('request');
			$session 			= $request->getSession();
			$searchParams		= array();
			$page 				= !empty($_GET['page']) ? (int) $_GET['page'] : 1;
			$limit = !empty($_GET['limit']) ? (int)$_GET['limit'] : 20;
			$searchParams['limit']		= $limit;
			$current_page = !empty($_GET['page'])?(int)$_GET['page']:1;
			$searchParams['start']		= $limit*($current_page - 1);
			
			$searchParams['GroupID']	= $request->request->get('roleid');
			
			$key 	= $session->get(AuthorizationProvider::CURRENTUSER);
			$objUserInfor = json_decode(RedisManager::getValue($key), true);
			
			$searchParams['ModuleID'] = $objUserInfor['GroupID'];
			$objActionList = RestApi::getData('ums_permission/getlist', $searchParams);
			//echo '<pre>';var_dump($objActionList);die;
			$data['datas'] = $objActionList['data'];
			
			
			$arrGroupID = explode(",", $objUserInfor['GroupID']);
			
			if(self::recursive_array_search(AuthorizationProvider::ROLE_ADMIN, $arrGroupID)) {
				$data_group = RestApi::getData('ums_group', array('limit'=>150));
			} else {
				$data_group = RestApi::getData('ums_group', 
					array('ParentGroupID'=>$objUserInfor['GroupID'], 'limit'=>150));
			}
			
			// Get list role
			$data['data_group'] = $data_group['data']['datas'];
			// Get module
			//$data_module = RestApi::getData('Ums_module');
			//$data['data_module'] = $data_module['data']['datas'];
			
			$data['data_search'] = $searchParams;
			
			// Paging
			$totalRows = $objActionList['totalRows'];
            $pg 		= new Pagination($totalRows, $limit, $page);
            $page_div 	= $pg->render();
			$data['pagination']	= $page_div;
			
			return $this->render('permision/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }

	private static function recursive_array_search($needle,$haystack) {
		foreach($haystack as $a) {
	        if ($needle === $a) 
				return true;
	    }
	    return false;
	}

	public function savePermissionAction() {
		try {
            $request 	= $this->get('request');
			$GroupID = $request->request->get('roleid');
			$ActionID = $request->request->get('actions');
			
            $reqdata = array(
				'GroupID' => $GroupID,
				'ActionID' => $ActionID
			);
			//var_dump($reqdata);die;
            $resultPost = array();
			$resultPost = RestApi::postData('ums_permission/update-multi', $reqdata);
			//var_dump($resultPost);die;
			return new Response(json_encode($resultPost));
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
}
