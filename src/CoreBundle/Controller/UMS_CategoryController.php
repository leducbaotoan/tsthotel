<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\Utils\PhpUtility;
use CoreBundle\Lib\Security\AuthorizationProvider;
use CoreBundle\Lib\Log\Logger;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Cache\RedisManager;
use CoreBundle\Lib\Security\Browser;
use CoreBundle\Lib\Config\ConfigManager;

/**
 * UMS_CategoryController Controller 
 * @author KhoaNT
 */
class UMS_CategoryController extends Controller
{
	private static function mappingFields() {
        return array(
        	'CategoryID' => 'This category name is existed',
		);
    }
	
	/**
	 * Get menu list
	 */
	public function getTreeMenuAction() {
		try
		{
			$request	= $this->get('request');
			$session 	= $request->getSession();
			$username 	= $session->get(AuthorizationProvider::CURRENTUSER);
			// Get client info
			$respmenu = array();
			$objUserInfor = json_decode(RedisManager::getValue($username), true);
			//echo '<pre>';var_dump($objUserInfor);die;
			if(isset($objUserInfor['menu']) && count($objUserInfor['menu'])) {
				$respmenu = $objUserInfor['menu'];
			} else {
				// Ge Menu for this user
				$rsMenu = RestApi::getData('category/get-tree-menu', 
											array('GroupID'=>$objUserInfor['GroupID']));
				$arrMenuData = $rsMenu['data'];
				//var_dump($arrMenuData);die;
				$i = 0;
				foreach ($arrMenuData as $value) {
					$items = $value['Items'];
					if(count($items)) {
						$respmenu[$i] = $value;
						$j = 0;
						foreach ($items as $subitem) {
							$subitem['Routing'] = $subitem['Routing'] != NULL ? 
								$this->generateUrl($subitem['Routing']) : 
								$this->generateUrl('home');
							$items[$j] = $subitem;
							$j += 1;
						}
						
						$respmenu[$i]['Items'] = $items;
						$i += 1;
					}
				}

				$objUserInfor['menu'] = $respmenu;
				$browser = new Browser();
        		$objUserInfor['client_browser'] = $browser->getBrowser();
				$time_exp_login = ConfigManager::getConfig('time_exp_login');
				$objUserInfor['client_IP'] = PhpUtility::get_client_ip();
				//var_dump($rsFinalMenu);die;
				RedisManager::setValue($username, json_encode($objUserInfor), $time_exp_login);
			}

			$path_upload = ConfigManager::getConfig('path_upload');
			if(isset($objUserInfor['Avatar']))
				$pms_avarta = $path_upload.$objUserInfor['Avatar'];
			else
				$pms_avarta = $path_upload.'/default/icon-user-default.png';

			$data = array(
				'pms_menu'	   =>  $respmenu,
				'pms_PostOfficeID' => isset($objUserInfor['PostOfficeID']) ? $objUserInfor['PostOfficeID'] : "",
				'pms_PostOfficeName' => isset($objUserInfor['PostOfficeName']) ? $objUserInfor['PostOfficeName'] : "",
				'pms_ZoneID' => isset($objUserInfor['ZoneID']) ? $objUserInfor['ZoneID'] : "",
				'pms_GroupID' => $objUserInfor['GroupID'],
				'pms_fullname' => $objUserInfor['UserGroupName'],
				'pms_avarta'   => $pms_avarta,
			);
			return new Response(json_encode($data));
		}
    	catch(\Exception $ex)
    	{
    		$resp = new ServerResponse();
			$resp->addErrorInfo("System", array("message"=>$ex->getMessage()));
    		return new Response(json_encode($resp));
    	}  
	}
	
	/**
     * Get all menu
     */
    public function getMenuListAction()
    {
    	try {
    		$data = array();
	    	$request 					= $this->get('request');
			$session 					= $request->getSession();
			$searchParams				= array();
			$limit = !empty($_GET['limit']) ? (int)$_GET['limit'] : 20;
			$searchParams['limit']		= $limit;
			$current_page = !empty($_GET['page'])?(int)$_GET['page']:1;
			$searchParams['start']		= $limit*($current_page - 1);
			
			$searchParams['Level'] = 1;
			$searchParams['CategoryID']	= 
				$request->request->get('CategoryIDLv1') != NULL ? $request->request->get('CategoryIDLv1') : NULL;
			$data['CategoryID'] = $searchParams['CategoryID'];
			
			$objMenuList = RestApi::getData('category/getmenu', array());
			$data['datas'] = $objMenuList['data'];
			
			$data_action = RestApi::getData('ums_action', array('ActionType' => 1, 'limit'=>1000));
			$data['data_actions'] = $data_action['data']['datas'];
			
			return $this->render('menu/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }
	
	public function saveMenuLv1Action() {
		try {
            $resp 		= new ServerResponse();
            $request 	= $this->get('request');
			$session 	= $request->getSession();
			$categoryID = $request->request->get('menuidlv1');
			
            $reqdata = array(
				'DisplayName' => $request->request->get('DisplayName'),
				'Icon' => $request->request->get('Icon'),
				'OrderNum' => (int)$request->request->get('OrderNum')
			);
			
            $resultPost = array();

            if ($categoryID != "") { // Update
                $resultPost = RestApi::postData('ums_category/update/' . $categoryID, $reqdata);
                $resp->addActionType('update');
            } else { // Insert
            	$reqdata['CategoryID'] = $request->request->get('CategoryID');
            	$reqdata['Level'] = 1;
				$reqdata['ParentID'] = "";
				$reqdata['ActionID'] = "";
                $resultPost = RestApi::postData('ums_category/create', $reqdata);
                $resp->addActionType('insert');
            }

            if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingFields());
                $resp->addErrorInfo('System', array('message' => $message));
            }

            $resp->addUrlPrevious($this->generateUrl('menu_index'));
            return new Response(json_encode($resp));
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
	
	public function saveMenuLv2Action() {
		try {
            $resp 		= new ServerResponse();
            $request 	= $this->get('request');
			$session 	= $request->getSession();
			$categoryID = $request->request->get('menuidlv2');
			
            $reqdata = array(
				'ParentID' => $request->request->get('popmenulv1'),
				'DisplayName' => $request->request->get('DisplayName2'),
				'Icon' => $request->request->get('Icon2'),
				'OrderNum' => (int)$request->request->get('OrderNum2'),
				'ActionID' => $request->request->get('actionid'),
			);

            $resultPost = array();

            if ($categoryID != "") { // Update
                $resultPost = RestApi::postData('ums_category/update/' . $categoryID, $reqdata);
                $resp->addActionType('update');
            } else { // Insert
            	$reqdata['CategoryID'] = $request->request->get('CategoryID2');
            	$reqdata['Level'] = 2;
                $resultPost = RestApi::postData('ums_category/create', $reqdata);
                $resp->addActionType('insert');
            }

            if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingFields());
                $resp->addErrorInfo('System', array('message' => $message));
            }

            $resp->addUrlPrevious($this->generateUrl('menu_index'));
            return new Response(json_encode($resp));
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
}
