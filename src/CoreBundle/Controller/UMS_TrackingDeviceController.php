<?php
namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Log\Logger;

/**
 * UMS_TrackingDeviceController
 * @author KhoaNT
 */
class UMS_TrackingDeviceController extends Controller
{
    /**
     * Get all list user
     */
    public function getTrackingDeviceListAction()
    {
    	try {
    		$data = array();
	    	$request 					= $this->get('request');
			$session 					= $request->getSession();
			$searchParams				= array();
			$limit = !empty($_GET['limit']) ? (int)$_GET['limit'] : 20;
			$searchParams['limit']		= $limit;
			$current_page = !empty($_GET['page'])?(int)$_GET['page']:1;
			$searchParams['offset']		= $limit*($current_page - 1);
			
			$tukhoa = !empty($_GET['q']) ? $_GET['q'] : NULL;
			$searchParams['q'] = $tukhoa;
			
			$objData = RestApi::getData('Ums_trackingdevice', $searchParams);
			$data['list_data'] = $objData['data']['datas'];
			
			$totalRows = $objData['data']['totalRows'];
			$pg = new Pagination($totalRows, $limit, $current_page);
            $page_div = $pg->render();
			$data['pagging'] = $page_div;
			
			$data['keyword'] = $tukhoa; 
			
			return $this->render('trackingdevice/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }
}
