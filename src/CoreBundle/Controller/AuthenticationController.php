<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use CoreBundle\Lib\Security\AuthorizationProvider;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\Utils\PhpUtility;
use CoreBundle\Lib\Log\Logger;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Cache\RedisManager;
use CoreBundle\Lib\Security\Browser;
use CoreBundle\Lib\Config\ConfigManager;

class AuthenticationController extends Controller {

	public function loginAction() {
		return $this -> render('login/index.html.twig', array('error' => ''));
	}
	
	public function timeoutAction($errmsg, $msgType, $route) {
		switch ($msgType) {
			case -1:
				return $this -> render('timeout.html.twig', 
										array('errtitle' => 'Warning', 
											  'errmsg' => $errmsg, 
											  'backurl' => $this->generateUrl('login'), 
											  'btnName' => 'OK'));
			case -2:
				$resp = new ServerResponse();
				$resp->addErrorInfo('System', array('message' => $errmsg));
				return new Response(json_encode($resp));
			case -3:
				return $this -> render('timeout.html.twig', 
					array('errtitle' => 'Warning', 
						  'errmsg' => $errmsg, 
						  'backurl' => $route == '' ? $this->generateUrl('home'): $this->generateUrl($route), 
						  'btnName' => 'Back'));
			case -5:
				return $this -> render('timeout.html.twig', 
					array('errtitle' => 'Warning', 
						  'errmsg' => $errmsg, 
						  'backurl' => $this->generateUrl('home'), 
						  'btnName' => 'Back'));
			case -4:
				return $this -> render('login/index.html.twig', array('error'=>''));
			default:
				$resp = new ServerResponse();
				$resp->addErrorInfo('System', array('message' => $errmsg));
				return new Response(json_encode($resp));
		}
	}

	/**
	 * Login function
	 */
	public function siginAction(Request $request) {
		try {
    		$resp 			= new ServerResponse();
			$searchParams 	= array();
			$data 			= array();
			$session 		= $request->getSession();

			$searchParams['user_name'] 	  = $request->request->get('username');
			$searchParams['password'] = $request->request->get('password');
			
			$objLogin = null;
			$objLogin = RestApi::postData('account/login', $searchParams);
			$respData = array();
			$respData = $objLogin['data'];
			
			if ($objLogin['result'] != -1) {
				$session->set(AuthorizationProvider::CURRENTUSER, $objLogin['data']['UserGroupID']);
				$session->set(AuthorizationProvider::TOKEN_KEY, $objLogin['data']['TokenVal']);
				$resp->addUrlCurrent($this->generateUrl('home'));
				
				$respmenu = array();
				// Ge Menu for this user
				$rsMenu = RestApi::getData('category/get-tree-menu', array('GroupID'=>$respData['GroupID']));
		
				$i = 0;
				foreach ($rsMenu['data'] as $value) {
					$items = $value['Items'];
					if(count($items)) {
						$respmenu[$i] = $value;
						$j = 0;
						foreach ($items as $subitem) {
							$subitem['Routing'] = $subitem['Routing'] != NULL ? 
								$this->generateUrl($subitem['Routing']) : $this->generateUrl('home');
							$items[$j] = $subitem;
							$j += 1;
						}
						$respmenu[$i]['Items'] = $items;
						$i += 1;
					}
				}
				
				$browser = new Browser();
				$respData['Browser'] = $browser->getBrowser();
				$time_exp_login = ConfigManager::getConfig('time_exp_login');
				$respData['IP'] = PhpUtility::get_client_ip();
				
				$respData['WebNotiAppID'] = ConfigManager::getConfig('web_noti_app_id');
				$respData['WebNotiDomain'] = ConfigManager::getConfig('web_noti_sub_domain');

				// Set avarta
				$path_upload = ConfigManager::getConfig('path_upload');
				if(isset($respData['Avatar']))
					$pms_avarta = $path_upload.$respData['Avatar'];
				else
					$pms_avarta = $path_upload.'/default/icon-user-default.png';
				
				$respData['Avatar'] = $pms_avarta;
				$respData['Menu'] = $respmenu;
				
				// Get action roles
				$rsRoleAction = RestApi::getData('ums_permission', 
												  array('GroupID' => $respData['GroupID'],'limit'=>10000));
				$respData['actionRoles'] = $rsRoleAction['data']['datas'];
				
				// Set login infor to redis
				RedisManager::setValue($searchParams['user_name'], json_encode($respData), 432000);
				
				unset($respData['Browser']);
				unset($respData['CountryID']);
				unset($respData['ParentGroupID']);
				unset($respData['ProvinceID']);
				unset($respData['ProvinceName']);
				unset($respData['CountryName']);
				unset($respData['IP']);
				unset($respData['actionRoles']);
				
				$resp->setResponseData($respData);

				return new Response(json_encode($resp));
			} else {
				$resp->addErrorInfo('System', array('message' => "Tên đăng nhập hoặc mật khẩu không đúng"));
				return new Response(json_encode($resp));
			}
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
	}

	/**
	 * Sigout
	 */
	public function sigoutAction() {
		try {
    		// Delete all token and session
			// $request = $this -> get('request');
			// $session 		= $request->getSession();
			// //Return to login form
			// $username 	= $session->get(AuthorizationProvider::CURRENTUSER);
			// RedisManager::setValue($username, '', 1);
			// $request->getSession()->set(AuthorizationProvider::CURRENTUSER, '');
			// $request->getSession()->set(AuthorizationProvider::TOKEN_KEY, '');
			// session_destroy();
			// $response = $this->redirectToRoute('login');
			// return $response;
			
			
			// Delete all token and session
    		$resp 		= new ServerResponse();
			$request 	= $this -> get('request');
			$session 	= $request->getSession();
			//Return to login form
			$username 	= $session->get(AuthorizationProvider::CURRENTUSER);
			RedisManager::setValue($username, '', 1);
			$request->getSession()->set(AuthorizationProvider::CURRENTUSER, '');
			$request->getSession()->set(AuthorizationProvider::TOKEN_KEY, '');
			session_destroy();
			
			$resp->addUrlPrevious($this->generateUrl('login'));
            return new Response(json_encode($resp));
			
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            $resp->addErrorInfo('System', array('message' => "[Line ".$ex->getLine()."]. ".$ex->getMessage()));
            return new Response(json_encode($resp));
    	}
	}

	/**
	 * Action for setting excptionMsg variable in this class
	 */
	public function setExceptionMsg($exceptionMsg, $authCtrl) {
		$this -> exceptionMsg = $exceptionMsg;
		$this -> authCtrl = $authCtrl;
	}

	/**
	 * Action that throwing an error message
	 * @return json Object
	 */
	public function exceptionAction() {
		$file = "../app/Resources/views/timeout.html.twig";
		//return $this -> render('timeout.html.twig', array('error' => 'Invalid correct user or password.'));
		return new Response(file_get_contents($file));
	}

}
