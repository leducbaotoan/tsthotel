<?php
namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Log\Logger;

/**
 * UMS_PushNotifyController
 * @author KhoaNT
 */
class UMS_PushNotifyController extends Controller
{
    /**
     * Get all list user
     */
    public function getPushNotifyListAction()
    {
    	try {
    		$data = array();
	    	$request 					= $this->get('request');
			$session 					= $request->getSession();
			$searchParams				= array();
			$searchParams['limit']		= 10000;
			$searchParams['is_active']		= 1;
			
			$objData = RestApi::getData('employee', $searchParams);
			$data['list_data'] = $objData['data']['datas'];
			
			return $this->render('pushnotify/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }
	
	public function sendNotifiMessageAction() {
		try {
            $resp 		= new ServerResponse();
            $request 	= $this->get('request');
			$session 	= $request->getSession();
			$UserGroupID = $request->request->get('UserGroupID');
			$title = $request->request->get('title');
			$message = $request->request->get('message');
			
            $reqdata = array(
				'UserGroupID' => $UserGroupID,
				'Title' => $title,
				'Message' => $message,
				'MessageType'=>'Update_Version',
				'Module'=>'',
				'ModuleParams'=>'',
			);
            $resultPost = array();

            $resultPost = RestApi::postData('ums_pushnotify/sendnotify', $reqdata);
            $resp->addActionType('update');
			
            if ($resultPost['result'] == -1) {
                $resp->addErrorInfo('System', array('message' => "Không thể gửi thông báo đến người sử dụng"));
            }

			$resp->addUrlPrevious($this->generateUrl('pushnotify_index'));
            return new Response(json_encode($resp));
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
}
