<?php
namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Log\Logger;

/**
 * VersionController
 * @author KhoaNT
 */
class VersionController extends Controller
{
	private static function mappingFields() {
        return array(
        	'VersionID' => 'Tên ứng dụng'
		);
    }
	
    /**
     * Get all list user
     */
    public function getVersionListAction()
    {
    	try {
    		$data 			= array();
	    	$request 		= $this->get('request');
			$session 		= $request->getSession();
			$searchParams	= array();
			$limit			= isset($_GET['limit']) ? $_GET['limit'] : 20;
			$page			= isset($_GET['page']) ? $_GET['page'] : 1;
			
			$searchParams['limit']	= $limit;
			$searchParams['offset']	= $limit*($page - 1);

			// Search params
			$searchParams['q']	= 
				$request->query->get('q') != NULL ? $request->query->get('q') : NULL;
			
			//var_dump($searchParams);die;
			$objData = RestApi::getData('version', $searchParams);
			$data['list_data'] = $objData['data']['datas'];
			
			$totalRows = $objData['data']['totalRows'];
			$pg = new Pagination($totalRows, $limit, $page);
            $page_div = $pg->render();
			
			$data['data_search'] = $searchParams;
			$data['pg'] = $page_div;
			
			return $this->render('version/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }
	
	public function getOneVersionAction($id) {
		try {
			$request = $this -> get('request');
			$session = $request->getSession();
            $data_info = array();
			$data_info['data'] = array();
            if ($id != NULL || $id != 0) {
                $data_info = RestApi::getData('version/get/' . $id);
            }
            $data_info['title'] = 'Thêm mới';
            if ($id != NULL || $id != 0)
                $data_info['title'] = 'Cập nhật';
			
            return $this->render('version/edit.html.twig', $data_info);
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
	
	public function saveVersionAction() {
		try {
            $resp 		= new ServerResponse();
            $request 	= $this->get('request');
			$session 	= $request->getSession();
			$versionidhdn = $request->request->get('versionidhdn');
			
            $reqdata = array(
				'CurrentVersion' => $request->request->get('CurrentVersion'),
				'AllowVersion' => $request->request->get('AllowVersion')
			);
			
            $resultPost = array();
            if (!empty($versionidhdn)) { 		// Update
                $resultPost = RestApi::postData('version/update/' . trim($versionidhdn), $reqdata);
                $resp->addActionType('update');
            } else { 					// Insert
            	unset($reqdata['UpdatedBy']);
            	$reqdata['VersionID'] = $request -> request -> get("VersionID");
                $resultPost = RestApi::postData('version/create', $reqdata);
                $resp->addActionType('insert');
            }

            if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingFields());
                $resp->addErrorInfo('System', array('message' => $message));
            }

            $resp->addUrlPrevious($this->generateUrl('version_index'));
            return new Response(json_encode($resp));
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            $resp->addErrorInfo('System', array('message' => "[Line ".$ex->getLine()."]. ".$ex->getMessage()));
            return new Response(json_encode($resp));
        }
	}


}
