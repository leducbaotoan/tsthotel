<?php
namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CoreBundle\Lib\Security\AuthorizationProvider;
use CoreBundle\Lib\Security\SecurityManager;
use CoreBundle\Lib\Pagination\Pagination;
use CoreBundle\Lib\Utils\ServerResponse;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Log\Logger;
use CoreBundle\Lib\Cache\RedisManager;
use CoreBundle\Lib\Config\ConfigManager;
use CoreBundle\Lib\Utils\PhpUtility;


/**
 * UMS_tblUserAccountController
 * @author KhoaNT
 */
class UMS_tblUserAccountController extends Controller
{
	private static function mappingFields() {
        return array(
        	'UserGroupID' => ' Người dùng'
		);
    }
	
    /**
     * Get all list user
     */
    public function getUserListAction()
    {
    	try {
    		$data = array();
	    	$request 			= $this->get('request');
			$session 			= $request->getSession();
			$searchParams		= array();
			$limit				= !empty($_GET['limit']) ? (int)$_GET['limit'] : 20;
			$page 				= !empty($_GET['page']) ? (int) $_GET['page'] : 1;
            $offset 			= $limit * ($page - 1);
			
			$searchParams['limit']		= $limit;
			$searchParams['offset']		= $offset;
			
			$searchParams['q']	= !empty($_GET['q']) ? $_GET['q'] : NULL;
			
			$objMenuList = RestApi::getData('ums_useraccount', $searchParams);
			$data['datas'] = $objMenuList['data']['datas'];
			
			$data['data_search'] = $searchParams;
			
			// Paging
			$totalRows = $objMenuList['data']['totalRows'];
            $pg 		= new Pagination($totalRows, $limit, $page);
            $page_div 	= $pg->render();
			$data['pagination']	= $page_div;
			
			return $this->render('user/index.html.twig', $data);
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
    }

	public function getOneUserAction($id) {
		try {
			set_time_limit(3000);
            $data_info = array();
			$data_info['data'] = array();
			$data_info['title'] = 'Thêm mới';
			$data_info['action'] = 'new';
			
			$data_group = RedisManager::getValue('data_UMS_Group');
			$data_info['group_list'] = unserialize(gzuncompress(base64_decode($data_group)));
			//$data_group = RestApi::getData('ums_group', array('limit'=>100));
			//$data_info['group_list'] = $data_group['data']['datas'];
			
			//$data_office = RestApi::getData('postoffice', array('limit'=>1000, 'active'=>1));
			//$data_info['postoffice_list'] = $data_office['data']['datas'];
			$data_office = RedisManager::getValue('data_MM_PostOffices');
			$data_info['postoffice_list'] = unserialize(gzuncompress(base64_decode($data_office)));
			
			$data_member = RestApi::getData('ums_useraccount', array('limit'=>10000, 'FValid'=>1));
			$data_info['member_list'] = $data_member['data']['datas'];
			//$data_member = RedisManager::getValue('data_UMS_tblUserAccount');
			//$data_info['member_list'] = unserialize(gzuncompress(base64_decode($data_member)));
			
			//$data_customer = RedisManager::getValue('data_MM_Customers');
			//$data_info['customer_list'] = unserialize(gzuncompress(base64_decode($data_customer)));
			
            if ($id != NULL || $id != "") {
                $data = RestApi::getData('ums_useraccount/get/' . $id);
				//var_dump($data);die;
				$data_info['data'] = $data['data'];
				$data_info['title'] = 'Cập nhật';
				$data_info['action'] = 'update';
				//var_dump($data_info);die;
            } else {
            	$data_info['data'] = array('GroupID' => "", 'PostOfficeID' => "", 
            								'MemberOf'=>"", 'CustomerID'=>'');
            }
            
            return $this->render('user/edit.html.twig', $data_info);
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}
	
	public function saveUserAction() {
		try {
            $resp 		= new ServerResponse();
            $request 	= $this->get('request');
			$session 	= $request->getSession();
			$usergroupid = $request->request->get('usergroupid');
			
			$arrPostOffice = $request->request->get('postofficeid');
			$PostOffice = "";
			if(count($arrPostOffice)){
				$PostOffice = implode(",", $arrPostOffice);
			} 
			
			$arrGroupId = $request->request->get('groupid');
			$groupIDs = "";
			if(count($arrGroupId)){
				$groupIDs = implode(",", $arrGroupId);
			} 
			$path_upload = ConfigManager::getConfig('path_upload');
            $reqdata = array(
				'Description' => trim($request->request->get('description')),
				'UserGroupName' => $request->request->get('useraccountname'),
				'PostOfficeID' => $PostOffice,
				'MemberOf' => $request->request->get('memberof'),
				'FValid' => intval($request->request->get('FValid')),
				'GroupID' => $groupIDs,
				'FAdm' => intval($request->request->get('FAdm')),
				'FSpv' => intval($request->request->get('FSpv')),
				'MustChangePwd' => intval($request->request->get('MustChangePwd')),
				'CantChangePwd' => intval($request->request->get('CantChangePwd')),
				'PwdNeverExpire' => intval($request->request->get('PwdNeverExpire')),
				'IsCustomerUser' => intval($request->request->get('IsCustomerUser')),
				'CustomerID' => $request->request->get('customerid'),
				'IsAllowLoginWeb' => intval($request->request->get('IsAllowLoginWeb')),
				'IsAllowLoginMobile' => intval($request->request->get('IsAllowLoginMobile')),
				'FGroup'=>0
			);
			
            $resultPost = array();
			//var_dump($reqdata);die;

            if (trim($usergroupid) != "") { // Update
                $resultPost = RestApi::postData('ums_useraccount/update/' . trim($usergroupid), $reqdata);
                $resp->addActionType('update');
            } else { // Insert
            	$reqdata['UserGroupID'] = $request->request->get('usergroupcode');
				$reqdata['Password'] = mb_strtoupper(sha1(trim($request->request->get('password'))));
				$reqdata['Avatar'] = $path_upload.'/default/icon-user-default.png';
				
                $resultPost = RestApi::postData('ums_useraccount/create', $reqdata);
                $resp->addActionType('insert');
            }
            if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingFields());
                $resp->addErrorInfo('System', array('message' => $message));
            } else {
            	$resp->Success = true;
            }
			
            $resp->addUrlPrevious($this->generateUrl('user_index'));
            return new Response(json_encode($resp));
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}

	public function profileUserAction() {
		try {
            $dataprofile = array();
			$request 	= $this->get('request');
			$session 	= $request->getSession();
			$tokenKey   = $session->get(AuthorizationProvider::CURRENTUSER);
			$userInfo   = SecurityManager::getClientInfo($tokenKey);
			$path_upload = ConfigManager::getConfig('path_upload');
			
            $data = RestApi::getData('ums_useraccount/get/'.$userInfo['UserGroupID']);
			
			$dataprofile = $data['data'];
			if(isset($dataprofile['Avatar']))
				$dataprofile['Avatar'] = $path_upload.$dataprofile['Avatar'];
			else
				$dataprofile['Avatar'] = $path_upload.'/default/icon-user-default.png';
			
            return $this->render('user/profile.html.twig', $dataprofile);
        } catch (\Exception $ex) {
            Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
        }
	}

	public function saveProfileUserAction() {
		try {
    		$resp = new ServerResponse();
			$request 	= $this->get('request');
			$userID 	= $request->request->get('UserGroupID');

			$reqdata = array(
				'UserGroupName' => $request->request->get('UserGroupName')
			);
			// check confirm password
			$paramCheck = array('UserGroupID'=>$userID, 
					            'oldpassword'=>$request->request->get('oldpassword'));
			$flagChangePass = FALSE;
			if($request->request->get('newpassword') !== "" && $request->request->get('oldpassword') !== "") {
				$flagConfirm = RestApi::postData('account/checkmatchpassword', $paramCheck);
				if($flagConfirm['result'] == -1) {
                	$resp->addErrorInfo('System', array('message' => 'Mật khẩu cũ không đúng'));
					return new Response(json_encode($resp));
				}
				$flagChangePass = TRUE;
				$reqdata['Password'] = mb_strtoupper(sha1(trim($request->request->get('newpassword'))));
			}
			
			$query = array();
			if(isset($_FILES["fileImage"])) {
				$fileName = $_FILES["fileImage"]["name"];
				$reqdata['fileName'] = $fileName;
				$fileContent = urlencode(file_get_contents($_FILES["fileImage"]["tmp_name"]));
				$query = PhpUtility::uploadFile($_FILES["fileImage"]["tmp_name"], 
									  			$fileName, $userID);
			}

			if(count($query) > 0) {
				$reqdata['Avatar'] = $query['data']['url'];
			}

			$resultPost = RestApi::postData('ums_useraccount/update/' . trim($userID), $reqdata);
			
			if ($resultPost['result'] == -1) {
                $message = PhpUtility::renderErrorDetail($resultPost['error'], self::mappingFields());
                $resp->addErrorInfo('System', array('message' => $message));
            } else {
            	$resp->Success = true;
            }

			if($flagChangePass)
				$resp->addUrlPrevious($this->generateUrl('login'));
			else
				$resp->addUrlPrevious($this->generateUrl('user_profile'));
			
			return new Response(json_encode($resp));
    	} catch(\Exception $ex) {
    		Logger::errLog($ex);
            return $this->render('exception/error.html.twig');
    	}
	}
	
}
