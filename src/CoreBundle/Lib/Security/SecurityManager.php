<?php
namespace CoreBundle\Lib\Security;

use CoreBundle\Lib\Log\Logger;
use CoreBundle\Lib\Utils\PhpUtility;
use CoreBundle\Lib\Cache\RedisManager;
use CoreBundle\Lib\Utils\DataUtility;
use CoreBundle\Lib\Security\AuthorizationProvider;
use CoreBundle\Lib\RestApi\RestApi;
use CoreBundle\Lib\Config\ConfigManager;

/**
 * Contains function to manage the security
 * @author Khoa Nguyen
 * @package SecurityManager
 */
final class SecurityManager {

	private static $_data;
	private static $_client_info;
	const LIFE_TIMEOUT_AUTH = 9000;
	// 15 min out session
	/*
	 * sync data with cache
	 * */
	protected static function getData($tokenKey) {
		if (empty(self::$_data)) {
			$data = json_decode(RedisManager::getValue($tokenKey), true);
			if($data != NULL)
				self::$_client_info = $data["$tokenKey"]['clientInfo'];
		}
		return self::$_client_info;
	}

	/*
	 * generate a random API key
	 */
	private static function generateAPIKey() {
		return PhpUtility::generateUID();
	}

	public static function init($tokenKey) {
		self::getData($tokenKey);
	}

	/**
	 * check user has logged
	 * @param $username  String --> account name
	 * @return api key else return null
	 */
	public static function checkUserLogged($tokenKey, $tokenVal) {
		$data = json_decode(RedisManager::getValue($tokenKey), true);
		if(empty($tokenVal) || empty($data)) {
			return false;
		}
		
		if($tokenVal !== $data['TokenVal']) {
			return false;
		}
			
		return true;
	}

	public static function getClientInfo($tokenKey) {
		$objUserInfo = json_decode(RedisManager::getValue($tokenKey), true);
		return $objUserInfo;
	}
	
	public static function saveLastFunction($tokenKey, $controllerName, $actionName, $route) {
		$objUserInfo = json_decode(RedisManager::getValue($tokenKey), true);
		$objUserInfo["controller"] = $controllerName;
		$objUserInfo["action"] = $actionName;
		$objUserInfo["route"] = $route;
		RedisManager::setValue($tokenKey, json_encode($objUserInfo));
	}
	
	public static function authorization($controllerName, $actionName, $tokenKey) {
		// Get action role from redis
		$objUserInfor = json_decode(RedisManager::getValue($tokenKey), true);		
		if($objUserInfor != NULL) {
			$actionRoles = array();
		
			$actionRoles = $objUserInfor['actionRoles'];
			
			$arrGroupID = explode(",", $objUserInfor['GroupID']);
			if(self::recursive_array_search(AuthorizationProvider::ROLE_ADMIN,$arrGroupID)){
				return 1;
			}

			$founditem = -1;
			foreach ($actionRoles as $value) {
				if(strtoupper($value['ActionID']) == strtoupper($actionName)) {
					$founditem = 1;
				}
			}
			return $founditem;
		} else {
			return 2;
		}
		
	}
	
	private static function recursive_array_search($needle,$haystack) {
	    foreach($haystack as $a) {
	        if ($needle === $a) 
				return true;
	    }
	    return false;
	}
}
