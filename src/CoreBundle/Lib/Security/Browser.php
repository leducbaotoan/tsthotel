<?php
namespace CoreBundle\Lib\Security;

/**
 * Detech Client's Browser
 *
 */
class Browser {

	private $browsers;
	private $useragent;

	function __construct() {
		$this -> useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
		$this -> browsers = array('firefox', 'chrome', 'opera', 'msie', 'safari', 'blackberry', 'trident');
	}

	/**
	 * Get browser
	 */
	public function getBrowser() {
		foreach ($this->browsers as $key => $browser) {
			if (strstr($this -> useragent, $browser) != FALSE) {
				return $browser;
			}
		}
	}
}
?>