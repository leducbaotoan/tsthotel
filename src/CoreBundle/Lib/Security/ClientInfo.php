<?php
namespace CoreBundle\Lib\Security;

use Symfony\Component\HttpFoundation\Request;
use CoreBundle\Lib\Security\Browser;

class ClientInfo {
	private $clientTimeZone;
	private $clientIP;
	private $timeLogin;
	private $clientBrowser;
	private $clientDevice;
	private $username;
	private $platform;
	private $roleid;
	private $lastFunctionUsed;

	function __construct() {
		$request = new Request();
		$browser = new Browser();
		$this -> clientBrowser = $browser -> getBrowser();
		$this -> clientIP = $request -> getClientIp();

	}

	public function getClientTimeZone() {
		return $this -> clientTimeZone;
	}

	public function getTimeLogin() {
		return $this -> timeLogin;
	}

	public function getClientBrowser() {
		return $this -> clientBrowser;
	}

	public function getClientDevice() {
		return $this -> clientDevice;
	}

	public function getUsername() {
		return $this -> username;
	}

	public function getRoleid() {
		return $this -> roleid;
	}

	public function getLastFunctionUsed() {
		return $this -> lastFunctionUsed;
	}

	public function setLastFunctionUsed($functionName) {
		$this -> lastFunctionUsed = $functionName;
	}

	public function setUsername($username) {
		$this -> username = $username;
	}

	public function setRoleid($roleid) {
		$this -> roleid = $roleid;
	}

	public function setTimelogin($timeLogin) {
		$this -> timeLogin = $timeLogin;

	}

	public function setTimeClientZone($clientTimeZone) {
		$this -> clientTimeZone = $clientTimeZone;
	}
}
?>