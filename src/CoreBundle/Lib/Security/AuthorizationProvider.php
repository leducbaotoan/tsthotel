<?php
namespace CoreBundle\Lib\Security;

/**
 * Business class for Authorization Provider
 * 
 * @author Khoa Nguyen
 * 
 */
class AuthorizationProvider
{
	const ROLE_ALL	 = -1;	
	const ROLE_ADMIN = "ADMIN";
	const ROLE_MANAGER = 2;
	const ROLE_NORMALUSER = 3;
	const ROLE_ADIDASOURNER = 4;
	const AUTHORIZATION_PROVIDER_MEMORY = "Memory";
	const AUTHORIZATION_PROVIDER_DB = "DB";
	const CURRENTUSER 				= 'Current_User';
	const FULLNAME 					= "admin";
	const TOKEN_KEY					= "";
	const IMAGE_FILE				= "";
	const POSTOFFICE_ID				= "postoffice_id";
	const POSTOFFICE_NAME			= "postoffice_name";
	const ZONE_ID					= "zone_id";
	const GROUP_ID					= "group_id";
	
	
	public static function authAction($action) {
		$common_actions = array('sigout', 'login', 'sigin', 'getTreeMenu', 'show',
								'home', 'profileUser', 'saveProfileUser', 'getDataBy','downloadFile','pricingColdenterexcel','importExcelColdenterexcel','import2ExcelColdenterexcel',
								'downloadPdf', 'downloadExcel','loadData','loadMailerIDMailersManage','ajaxReportForDepartmentConfirmAction','checkReportForDepartmentConfirmAction',
                                'loadInfoCustomerMailersManage','getProvinceMailersManageList','getDistrictMailersManageList','nccMailerDeliveryBook','wardsMailerDeliveryBook','ajaxEmployeeQualitys','addrowEmployeeQualitys',
                                'getWardMailersManageList ','getQuarterMailersManageList','getAddressMailersManageList','getPostCodeMailersManageList','districtMailerDeliveryBook','provinceMailerDeliveryBook',
                                'loadFuelFeeMailersManageList','getPriceMailersManage','getPriceOutSideMailersManage','loadFarRegionFeeMailersManageList','getrow2ReceiveMail','getrowReceiveMail','addrow','addrowMailerDeliveryBook',
                                'getPriceCODMailersManage','checkAddNewMailerIDExists','getAutoIDAddNewMailer','getMailerAddNewMailer','addrow3PackingMailer','postofficePackingMailer','createCodePackingMailer','addrow2PackingMailer','addrowPackingMailer',
                                'getChildPostOfficeAddNewMailer','getParentPostOfficeAddNewMailer','configexchangerevenuenewcustomer_ajax','approvenewcustomer_ajax',
								'createPackingMailer','printAddNewMailer');
		if (!in_array($action, $common_actions)) {
		   	return true;
		} else {
			return false;
		}
	}
}
