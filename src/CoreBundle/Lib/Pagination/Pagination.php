<?php
namespace CoreBundle\Lib\Pagination;
class Pagination{
    protected $_page;
    protected $_limit;
    protected $_total_rows;
    protected $path='/';
    protected $_pages = 0;
    public $_var = 'page';
    public $_num = 7;
    public function __construct($totalRows,$limit,$currentPage) {
        $this->_limit = $limit;
        $this->_page = $currentPage;
        $this->_total_rows = $totalRows;
        $this->cal();
    }
    
    public function cal(){
        $this->path = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
        if($this->_total_rows%$this->_limit){
            $this->_pages = (int)($this->_total_rows/$this->_limit) + 1;
        }
        else{
            $this->_pages = (int)($this->_total_rows/$this->_limit);
        }
    }


    public function render($echo=false){
        if($this->_total_rows<=$this->_limit){
            return '';
        }
        $total = intval($this->_total_rows);
        $limit = intval ($this->_limit);
        //echo "Tổng: ".$total."_".$limit; die;
        $number_last_page = ceil($total/$limit);
        $_GET[$this->_var] = 1;
        $frist_url = $this->path.'?'.http_build_query($_GET);
        $_GET[$this->_var] = $number_last_page;
        $last_url = $this->path.'?'.http_build_query($_GET);
        $str = '<ul class="pagination">';
        $pre = $this->_page-1;
        $_GET[$this->_var] = $pre;
        $pre_url = $this->path.'?'.http_build_query($_GET);
        
        $disable_prev = '';
        $title_prev = 'Trang trước đó';
        $title_next = 'Trang kế tiếp';
        $disable_next = '';
        if($pre<=0){
            $pre_url = 'javascript:void(0)';
            $disable_prev = 'dis-btn-pag';
            $title_prev = 'Không còn trang nào';
            $frist_url = 'javascript:void(0)';
        }
        $str.="<li title='Trang đầu tiên'><a href='".$frist_url."'><i class='fa fa-step-backward'></i></a></li><li title='".$title_prev."'><a class='".$disable_prev."' href='$pre_url'><i class='fa fa-backward'></i></a></li>";
        $nxt = $this->_page+1;
        $_GET[$this->_var] = $nxt;
        $nxt_url = $this->path.'?'.http_build_query($_GET);
        if($nxt>$this->_pages){
            $nxt_url = 'javascript:void(0)';
            $disable_next = 'dis-btn-pag';
            $title_next = 'Không còn trang nào';
        } 
        unset($_GET[$this->_var]);
        $url = $this->path.'?'.http_build_query($_GET);
        $status_url = 1;
        if(empty($_GET)){
            $status_url = 0;
        }
        $str.='<li><a style="padding:0px 0px;"><input status-url="'.$status_url.'" id="page-list" data-var="'.$this->_var.'" data-url="'.$url.'" style="border:0px none;padding:6px 6px;width:40px;text-align:center;display:inline-block" value="'.$this->_page.'" type="text" min="0"></a></li>';
        $str.="<li title='".$title_next."'><a class='".$disable_next."' href='$nxt_url'><i class='fa fa-forward'></i></a></li><li title='Trang cuối cùng'><a href='".$last_url."'><i class='fa fa-step-forward'></i></a></li><li style='margin-left: 20px; line-height: 30px;'>Trang ".$this->_page."/".$number_last_page."</li>";
        $str.= '</ul>';
        if($echo===true){
            echo $str;
        }
        else{
            return $str;
        }
    }
}