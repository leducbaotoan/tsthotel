<?php
namespace CoreBundle\Lib\Config;

use CoreBundle\Lib\Utils\DataUtility;

/**
 * Contains function to read config. Default config is JSON file
 * Config file must be store outside of web directory
 * Config must be cached
 * @author Khoa Nguyen
 * @package ConfigManager
 */
final class ConfigManager {

	public static $configs = array();
	
    public static function init()
    {
		$kernel = $GLOBALS['kernel'];
        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }

		$container = $kernel->getContainer();
		self::$configs['api_host'] = $container->getParameter('api_host');
		// self::$configs['path_log'] = $container->getParameter('path_log');
		// self::$configs['path_tmp'] = $container->getParameter('path_tmp');
		// self::$configs['redis_host'] = $container->getParameter('redis_host');
		// self::$configs['redis_port'] = $container->getParameter('redis_port');
		// self::$configs['redis_pass'] = $container->getParameter('redis_pass');
		// self::$configs['time_exp_login'] = $container->getParameter('time_exp_login');
		// self::$configs['path_pear'] = $container->getParameter('path_pear');
		// self::$configs['path_upload'] = $container->getParameter('path_upload');
		// self::$configs['web_noti_app_id'] = $container->getParameter('web_noti_app_id');
		// self::$configs['web_noti_sub_domain'] = $container->getParameter('web_noti_sub_domain');

		//DataUtility::setEntityManager($container);	 
    }
    
	
	public static function getConfig($key) {
		return self::$configs[$key];
	}
}