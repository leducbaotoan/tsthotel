<?php

namespace CoreBundle\Lib\Utils;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ResultSetMapping;
use Doctrine\DBAL\Connection;

/**
 * Contains all db-related utility functions
 * @author Khoa Nguyen
 * @package DataUtility 
 */
class DataUtility
{
    /**
     * Entity Manager
     * @access private
     * @var EntityManager
     */
	private static $_entityManager = NULL;
	
     /**
     * Get the Entity Manager
     * @return EntityManager 
     */
	public static function getEntityManager()
	{		
		return self::$_entityManager;
	}
     /**
     * Set the Entity Manager
     * @param $container : \Symfony\Component\DependencyInjection\ContainerInterface 
     * @return none 
     */
	public static function setEntityManager(\Symfony\Component\DependencyInjection\ContainerInterface $container)
	{
		$connection = $container->get(sprintf('doctrine.dbal.%s_connection', 'default'));
	    $connection->close();
	
	    $refConn = new \ReflectionObject($connection);
	    $refParams = $refConn->getProperty('_params');
	    $refParams->setAccessible('public'); //we have to change it for a moment
	
		$params = $refParams->getValue($connection);
		
	    $refParams->setAccessible('private');
	    $refParams->setValue($connection, $params);
	   	$container->get('doctrine')->resetManager('default');
		
		self::$_entityManager = $container->get('doctrine.orm.entity_manager');
	}
	
    /**
     * Start a SERIALIZABLE transaction
     * @return none 
     */
	public static function startTransaction()
	{
		if(self::$_entityManager != NULL)
		{
			$conn = self::$_entityManager->getConnection();
			//set Isolation level to Serializabe
			$conn->setTransactionIsolation(Connection::TRANSACTION_SERIALIZABLE);
			$conn->beginTransaction();
		}
	}
	
	/**
     * Commit current transaction       
     * @return none 
     */	
	public static function commitTransaction()
	{
		if(self::$_entityManager != NULL)
		{		
			$conn = self::$_entityManager->getConnection();
			$conn->commit();
		}		
	}
	
	 /**
     * Rollback current transaction       
     * @return none 
     */	
	public static function rollbackTransaction()
	{
		if(self::$_entityManager != NULL)
		{
			$conn = self::$_entityManager->getConnection();
			$conn->rollback();
		}		
	}
}
