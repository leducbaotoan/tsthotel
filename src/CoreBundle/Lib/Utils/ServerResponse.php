<?php
namespace CoreBundle\Lib\Utils;


/**
 * Class that contains data replied from server
 * @author Khoa Nguyen
 * @package ServerResponse 
 */
class ServerResponse
{
    /**
     * Operation is success or not
     * @access public
     * @var $Success
     */	
	public $Success = true;
	
	public $UrlPrevious = '';
	public $UrlCurrent = '';
	public $ActionType = '';

    /**
     * Array contains all error info
     * @access public
     * @var $ErrorInfo
     */	
	public $ErrorInfo = NULL;
	
    /**
     * Array contains all replied data
     * @access public
     * @var $Data
     */	
	public $Data = array();
	
	
	 /**
     * Add an error info into ErrorInfo array & set Success to false
     * @param $errField : Field name
     * @param $errCode : Error code 
     * @return none 
     */
	public function addErrorInfo($errField, $errCode)
	{
		if($this->ErrorInfo == NULL)
			$this->ErrorInfo = Array();
		
		$this->Success = false;
		
		$this->ErrorInfo[$errField]=$errCode;
	}
	
	public function addUrlCurrent($url)
	{
		$this->UrlCurrent = $url;
	}
	
	public function addUrlPrevious($url)
	{
		$this->UrlPrevious = $url;
	}
	
	public function addActionType($action)
	{
		$this->ActionType = $action;
	}

 	/**
     * Add an error info into ErrorInfo array & set Success to false
     * @param $errField : Field name
     * @param $errCode : Error code 
     * @return none 
     */
	public function setResponseData($data)
	{
		$this->Data = $data;
	}
	
	
}
