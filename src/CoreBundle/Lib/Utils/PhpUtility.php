<?php

namespace CoreBundle\Lib\Utils;

use CoreBundle\Lib\Config\ConfigManager;

/**
 * Contains all db-related utility functions
 * @author Khoa Nguyen
 * @package DataUtility 
 */
class PhpUtility
{
     /**
     * Get the server timezone
	  * 
     * @return string  '+0700' 
     */
	public static function getServerTimeZone()
	{
		$cmd	= 'date +%z';
		return exec($cmd);	
	}
	
	/**
     * Copy Object to Object
	  * 
     * @return destination object
     */
	public static function copyObject($source,$destination)
	{
		$reflectSource 		= new \ReflectionClass($source);
		$propsSource   		= $reflectSource->getProperties();
		
		$reflectDestination = new \ReflectionClass($destination);
		$propsDestination   = $reflectDestination->getProperties();
		
		foreach ($propsSource as $propSource) {
			foreach($propsDestination as $propDestination){
				if($propSource->getName() == $propDestination->getName() && $propSource->getName() != 'id'){						
					$field 	= ucfirst($propSource->getName());
					eval("if(\$source->get$field()!=null){\$destination->set$field(\$source->get$field());}");
					break;					
				}
			}			
		}
		return $destination;
	}
	
	 /**
	  * 
	  */
	  public static function convertValueType($objValue, $type, $dbSource=FieldMap::DB_MYSQL)
	  {
	  	$value = $objValue;
	  	switch ($type)
		{
			case FieldMap::DATATYPE_DATETIME:
				if ($dbSource==FieldMap::DB_MONGO)
				{
					if ($value!=null && is_a($value, 'DateTime'))
						$value = new \MongoDate($value->getTimestamp());
					else
						$value = new \MongoDate();
				}
				break;
		}
		return $value;
	  }
  /**
   * Convert MongoDate to Php Datetime
   */
   public static function convertMongoDT2PhpDT($date)
   {
   		$timeZone = date("O");
		$t1 = substr($timeZone, 0, 1);
		$t2 = substr($timeZone, 1, 2);
		$t3 = substr($timeZone, 3, 2);
		  
   		$sec = $date->sec;
		
		if ($t1=="+") {
			$sec += (((int)$t2)*60*60);
			$sec += (((int)$t3)*60);
		}
		
		$serverDate = new \DateTime();
		
		$newDate = new \DateTime("@$sec");
		$newDate->setTimezone($serverDate->getTimezone());
			
		return $newDate; 
   }
   
   /**
    * Generate a random string
    * @var $length: lenght of string need to generate 
    */
   public static function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}
   
   /**
    * Validate Account Name
    */
    public static function isvalidAccountname($accountname, &$msg)
	{
		if (!preg_match('/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/', $accountname))
		{
			$msg = "Account name is invalid, Account name must start with a alphabet character.";
			return false;
		}
		return true;
	}

   /**
    * Validate Password Rule
    */
    public static function isvalidPassword($password, &$msg)
	{
		if (strlen($password)< 8)
		{
			$msg = "The least number of characters that a password for a user account is 8.";
			return false;
		}
		return true;
	}
    /**
     * Check array is an associate array or not
     * @return true: associate array and false otherwise
     */
    public static function isAssocArray($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }       
    
    
    /**
     * Generate Unique ID
     * @return ramdom unique string
     */    
     public static function generateUID() {
         
         $uuid = "";
         mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
         $charid = strtoupper(md5(uniqid(rand(), true)));
         $hyphen = chr(45);// "-"
         $uuid = substr($charid, 0, 8).$hyphen
             .substr($charid, 8, 4).$hyphen
             .substr($charid,12, 4).$hyphen
             .substr($charid,16, 4).$hyphen
             .substr($charid,20,12);
        return strtolower($uuid);
	}
	 
	public static function checkValExtension($file, $arrExtension) {
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		foreach ($arrExtension as $value) {
			if($ext === $value) {
				return true;
			}
		}
		return false;
	}
	
	public static function uploadFile($fileContent, $fileName, $folderStore) {
		$host = ConfigManager::getConfig('api_host');
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_VERBOSE, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
	    curl_setopt($ch, CURLOPT_URL, $host . 'common/uploadfile/');
	    curl_setopt($ch, CURLOPT_POST, true);

	    $post = array(
	        "file_box"=>"@$fileContent",
	        "folderStore"=> $folderStore,
	        "fileName"=> $fileName
	    );
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
	    $response = curl_exec($ch);
		$finalRes = json_decode($response, true);
		return $finalRes;
	}
	
	public static function renderErrorDetail($errors, $mapping=array()){
		$message = array();
		if(!is_array($errors)){
			return "- ".$errors;
		}
		foreach($errors as $field=> $error){
			$field_name = $field;
			if(isset($mapping[$field])){
				$field_name = $mapping[$field];
			}
			foreach($error as $err){
				$message[] =  "- ".$field_name." ".$err;
			}
		}
		return implode("</br>", $message);
	}
	
	public static function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	public static function convert_number_to_words($number) {

		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'Không',
			1                   => 'Một',
			2                   => 'Hai',
			3                   => 'Ba',
			4                   => 'Bốn',
			5                   => 'Năm',
			6                   => 'Sáu',
			7                   => 'Bảy',
			8                   => 'Tám',
			9                   => 'Chín',
			10                  => 'Mười',
			11                  => 'Mười một',
			12                  => 'Mười hai',
			13                  => 'Mười ba',
			14                  => 'Mười bốn',
			15                  => 'Mười năm',
			16                  => 'Mười sáu',
			17                  => 'Mười bảy',
			18                  => 'Mười tám',
			19                  => 'Mười chín',
			20                  => 'Hai mươi',
			30                  => 'Ba mươi',
			40                  => 'Bốn mươi',
			50                  => 'Năm mươi',
			60                  => 'Sáu mươi',
			70                  => 'Bảy mươi',
			80                  => 'Tám mươi',
			90                  => 'Chín mươi',
			100                 => 'trăm',
			1000                => 'ngàn',
			1000000             => 'triệu',
			1000000000          => 'tỷ',
			1000000000000       => 'nghìn tỷ',
			1000000000000000    => 'ngàn triệu triệu',
			1000000000000000000 => 'tỷ tỷ'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
			);
			return false;
		}

		if ($number < 0) {
			return $negative . self::convert_number_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}

		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . self::convert_number_to_words($remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = self::convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= self::convert_number_to_words($remainder);
				}
				break;
		}

		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}

		return $string;
	}
    
    //TayLQ create function
    public static function utf8_to_ascii($str) {
        $chars = array(
            'a'	=>	array('ấ','ầ','ẩ','ẫ','ậ','Ấ','Ầ','Ẩ','Ẫ','Ậ','ắ','ằ','ẳ','ẵ','ặ','Ắ','Ằ','Ẳ','Ẵ','Ặ','á','à','ả','ã','ạ','â','ă','Á','À','Ả','Ã','Ạ','Â','Ă'),
            'e' =>	array('ế','ề','ể','ễ','ệ','Ế','Ề','Ể','Ễ','Ệ','é','è','ẻ','ẽ','ẹ','ê','É','È','Ẻ','Ẽ','Ẹ','Ê'),
            'i'	=>	array('í','ì','ỉ','ĩ','ị','Í','Ì','Ỉ','Ĩ','Ị'),
            'o'	=>	array('ố','ồ','ổ','ỗ','ộ','Ố','Ồ','Ổ','Ô','Ộ','ớ','ờ','ở','ỡ','ợ','Ớ','Ờ','Ở','Ỡ','Ợ','ó','ò','ỏ','õ','ọ','ô','ơ','Ó','Ò','Ỏ','Õ','Ọ','Ô','Ơ'),
            'u'	=>	array('ứ','ừ','ử','ữ','ự','Ứ','Ừ','Ử','Ữ','Ự','ú','ù','ủ','ũ','ụ','ư','Ú','Ù','Ủ','Ũ','Ụ','Ư'),
            'y'	=>	array('ý','ỳ','ỷ','ỹ','ỵ','Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            'd'	=>	array('đ','Đ'),
        );
        foreach ( $chars as $key=>$arr) {
            $str = str_replace( $arr, $key, $str);
        }
        return $str;
    }
	
	public static function genHeaderDate($fromdate, $todate)
	{
		$result = array();
		if($fromdate != NULL && $todate != NULL) {
			$arrFromDate = explode('-', $fromdate);
			$arrToDate = explode('-', $todate);
		
			if($arrFromDate[1] == $arrToDate[1]) {
				for ($i=$arrFromDate[0]; $i <= $arrToDate[0] ; $i++) { 
					$numOrder = sprintf('%02d', intval($i)).'/'.$arrFromDate[1];
					$result[] = $numOrder;
				}
			} else {
				for ($i=$arrFromDate[0]; $i <= 12 ; $i++) { 
					$numOrder = sprintf('%02d', intval($i)).'/'.$arrFromDate[1];
					$result[] = $numOrder;
				}
				
				for ($i=1; $i <= $arrToDate[0] ; $i++) { 
					$numOrder = sprintf('%02d', intval($i)).'/'.$arrToDate[1];
					$result[] = $numOrder;
				}
			}
		}
		
		return $result;
	}
	
	public static function get_index($data, $key, $default = null){
	    if(isset($data[$key])){
	        return $data[$key];
	    }
	    return $default;
	}
	public static function checkDateEndOfMouth($date){ // 08-2016
		$ms=explode('-',$date);
		$m=$ms[0];

		$nam=$ms[1].'-'.$ms[0].'01';
		if($m == '01' || $m == '03' || $m == '05' || $m == '07' || $m == '08' || $m == '10' || $m == '12'){
			$so_ngay=31;
		}
		elseif($m == '04' || $m == '06' || $m == '09' || $m == '11'){
			$so_ngay=30;
		}
		elseif($m == '02'){
			if(date('L', strtotime($nam))){
				$so_ngay=28;
			} else {
				$so_ngay=29;
			}
		}
		return $so_ngay;
	}
    
    //CONFIG REPORT STATUS
    public static function configReportStatus(){
        return array(
            0   => 'Khởi tạo',
            1   =>'Trưởng bưu cục lập chưa xác nhận xong',
            2   =>'Chờ phòng xác nhận',
            3   =>'Chờ thuyết minh',
            4   =>'Thuyết minh chưa xong',
            5   =>'Chờ trưởng bưu cục bị lập xác nhận',
            6   =>'Trưởng bưu cục bị lập xác nhận chưa xong',
            7   =>'Chờ phòng kết luận',
            8   =>'Phòng kết luận chưa xong',
            9   =>'Phòng kết luận xong',
            10  =>'Hủy'
        );
    }
}
