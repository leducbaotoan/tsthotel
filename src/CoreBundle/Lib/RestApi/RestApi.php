<?php
namespace CoreBundle\Lib\RestApi;
use CoreBundle\Lib\Config\ConfigManager;

set_time_limit(200);
/**
 * Class define for get and post data to API
 * @author Khoa Nhuyen
 */
class RestApi {

	private static $token = '247!@ExP2016';

	/**
	 * Get data
	 * @param $method function name
	 * @param $param the parameter to filter
	 */
	public static function getData($method, $param = array()) {
		$configManager = new ConfigManager;
		$configManager->init();
		$host = $configManager->getConfig('api_host');
		$curl = curl_init($host.$method.'?'.http_build_query($param));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'token:'.self::$token
		));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);

		// Execute
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			curl_close($curl);
			return NULL;
		}

		curl_close($curl);
                //var_dump($curl_response); die;
		$finalRes = json_decode($curl_response, true);
		return $finalRes;
	}

	/**
	 * Post data
	 * @param $method function name
	 * @param $param the data to post
	 *
	 */
	public static function postData($method, $param = array(),$multi=false) {
		$configManager = new ConfigManager;
		$configManager->init();
		$host = $configManager->getConfig('api_host');
		$curl = curl_init($host.$method);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'token:'.self::$token
		));
		curl_setopt($curl, CURLOPT_POST, true);
		$postvars ='';
		if($multi==false){
			foreach ($param as $p=>$v){
					if(is_array($v)){
							foreach ($v as $_v){
									$postvars .= $p . "=" . $_v . "&";
							};
					}
					else{
							$postvars .= $p . "=" . $v . "&";
					}
			}
		}
		else{
			$postvars = urldecode(http_build_query($param));
		}
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postvars);

		// Execute
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			curl_close($curl);
			return NULL;
		}

		curl_close($curl);
		$finalRes = json_decode($curl_response,true);
		if(json_last_error() == JSON_ERROR_NONE){
			return $finalRes;
		}
		else{
			echo "[Error api] $method $curl_response";
			//exit;
		}
		return $finalRes;
	}

}