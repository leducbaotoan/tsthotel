<?php
namespace CoreBundle\Lib\Cache;
use Redis;
use CoreBundle\Lib\Config\ConfigManager;
use CoreBundle\Lib\Log\Logger;

class RedisManager
{
	private static $redisClient;
	
	public static function openRedis() {
		try {
			if(self::$redisClient != NULL) {
	    		return; // is connected
	    	}
			
			$redis_host = ConfigManager::getConfig('redis_host');
			$redis_port = ConfigManager::getConfig('redis_port');
			$redis_pass = ConfigManager::getConfig('redis_pass');
	
			// open connection to redis server
	    	$redis = new Redis();
			$redis->connect($redis_host, $redis_port);
			$redis->auth($redis_pass);
			self::$redisClient = $redis;
			return true;
		} catch(\Exception $ex) {
			Logger::errLog($ex);
			return false;
		}
	}

	public static function setValue($key, $value, $time = 0) {
		$connect = self::openRedis();

		if ($connect === false) {
			return false;
		}
	
		$time = intval($time);
	
		if ($time > 0){
			$v_seted = self::$redisClient->set($key, $value, $time);
		} else {
			$v_seted = self::$redisClient->set($key, $value);
		}
		return $v_seted;
	}
	
	public static function getValue($key) {
		$connect = self::openRedis();
		if ($connect === false){
			return false;
		}
		try {
			$v_value = self::$redisClient->get($key);
			return $v_value;
		} catch(\Exception $ex) {
			Logger::errLog($ex);
		}
	}
} 
?>