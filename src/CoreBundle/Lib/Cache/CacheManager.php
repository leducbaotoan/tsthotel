<?php
namespace CoreBundle\Lib\Cache;

/**
 * Manage the cache. Can work with memcache(for now) or redis(later)
 * @author Khoa Nguyen
 * @package EntityMap 
 */
class CacheManager
{
	private static $CACHE_SERVER = "localhost";
	private static $CACHE_PORT = 11211;
	
	private static $_cacher;
	
	protected static function getCacher()
	{
		if(!isset(self::$_cacher))
		{	
			//init and connect to memcache here	
			self::$_cacher = new \Memcache();
			if (!self::$_cacher->connect(self::$CACHE_SERVER, self::$CACHE_PORT))
				throw new \Exception ("Could not connect Memcache Server (Server: ".self::$CACHE_SERVER.", Port: ".self::$CACHE_PORT.").");
		}
		
		return self::$_cacher;
	}
	
	public static function saveData($name, $obj) //save an object to cache
	{
		if (!self::getCacher()->set($name, $obj, false))
			throw new \Exception ("Could not set data to Memcache '$name'.");
	}
	
	public static function loadData($name) //load data from cache, return null if not found
	{
		return self::getCacher()->get($name);
	}
	
	public static function deleteCache($name) //delete a cahed object
	{
		if (!self::getCacher()->delete($key))
			throw new \Exception ("Could not delete Memcache '$name' from Server.");	
	}
	
	public static function removeAllCache()
	{
		if (!self::getCacher()->flush())
			throw new \Exception ("Could not flush memcache.");
	}
}
?>