<?php
namespace CoreBundle\Lib\Log;

use CoreBundle\Lib\Config\ConfigManager;

/**
 * Manage the logger. Use the mongodb to save the logs
 * @author Khoa Nguyen
 * @package Logger 
 */
class Logger
{
	const LOG_ALL = 0;
	const LOG_DEBUG = 1;
	const LOG_ERROR = 9;
	const LOG_CRITICAL = 99;
	private static $_logLevel = 0;
	private static $file_error_log = "pms_error_";
	private static $file_infor_log = "pms_infor_";
	
	/**
	 * log to db
	 */
	public static function errLog($exception) {

		$file = " IN ". $exception->getFile();
		$line = "[Line ".$exception->getLine()."]. ";
		$message = $line.$exception->getMessage().$file;
		$path_log = ConfigManager::getConfig('path_log');
		@chmod($path_log, 0777);
		file_put_contents($path_log.self::$file_error_log.date("Y-m-d").".log",
							date("Y-m-d H:i:s")."\t".$message."\n", FILE_APPEND | LOCK_EX);
		return false;
	}
	
	public static function infoLog($desc) {
		$path_log = ConfigManager::getConfig('path_log');
		@chmod($path_log, 0777);
		file_put_contents($path_log.self::$file_infor_log.date("Y-m-d").".log",
							date("Y-m-d H:i:s")."\t".var_export($desc,true)."\n", FILE_APPEND | LOCK_EX);
		return false;
	}
}